import cv2
import os
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
import joblib
import pandas as pd
from datetime import datetime
from flask import Flask, request, render_template

app = Flask(__name__, template_folder='C:/Users/ASUS/project1/face_recognition/templates')


class DatabaseManager:
    def __init__(self):
        self.current_date = datetime.now().strftime("%m_%d_%y")
        if not os.path.isdir('Attendance'):
            os.makedirs('Attendance')
        if not os.path.isdir('static'):
            os.makedirs('static')
        if not os.path.isdir('static/faces'):
            os.makedirs('static/faces')
        if f'Attendance-{self.current_date}.csv' not in os.listdir('Attendance'):
            with open(f'Attendance/Attendance-{self.current_date}.csv', 'w') as f:
                f.write('Name,Roll_no,Time')

    def extract_attendance(self):
        data_file = pd.read_csv(f'Attendance/Attendance-{self.current_date}.csv')
        names = data_file['Name']
        roll_no = data_file['Roll_no']
        _time = data_file['Time']
        size_of_file = len(data_file)
        return names, roll_no, _time, size_of_file

    def add_attendance(self, name):
        print(f"Attempting to add attendance for: {name}")
        username = name.split('_')[0]
        userid = name.split('_')[1]
        current_time = datetime.now().strftime("%H:%M:%S")
        data_file = pd.read_csv(f'Attendance/Attendance-{self.current_date}.csv')
        if int(userid) not in list(data_file['Roll_no']):
            with open(f'Attendance/Attendance-{self.current_date}.csv', 'a') as f:
                f.write(f'\n{username},{userid},{current_time}')
            print(f"Attendance marked for: {username} (ID: {userid})")

        self.convert_csv_to_excel()

    def convert_csv_to_excel(self):
        csv_file_path = f'Attendance/Attendance-{self.current_date}.csv'
        excel_file_path = f'Attendance/Attendance-{self.current_date}.xlsx'
        df = pd.read_csv(csv_file_path)
        df.to_excel(excel_file_path, index=False)
        print(f"Excel file saved at: {excel_file_path}")

    def get_all_users(self):
        userlist = os.listdir('static/faces')
        names = []
        roll_no_lst = []
        size = len(userlist)
        for i in userlist:
            name, roll_no = i.split('_')
            names.append(name)
            roll_no_lst.append(roll_no)
        return userlist, names, roll_no_lst, size


class FaceRecognitionSystem:
    def __init__(self, db_manager):
        self.db_manager = db_manager
        self.face_size = (50, 50)
        self.no_of_images = 50
        self.face_detector = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
        self.model_path = 'static/face_recognition_model.pkl'

    def extract_faces(self, img):
        if img is None:
            return []
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face_points = self.face_detector.detectMultiScale(gray, 1.2, 5, minSize=(20, 20))
        return face_points

    def identify_face(self, face_array):
        model = joblib.load(self.model_path)
        prediction = model.predict(face_array)
        return prediction

    def train_model(self):
        faces = []
        labels = []
        userlist = os.listdir('static/faces')

        for user in userlist:
            user_folder = f'static/faces/{user}'
            for img_name in os.listdir(user_folder):
                img = cv2.imread(f'{user_folder}/{img_name}')
                if img is None:
                    continue
                resized_face = cv2.resize(img, self.face_size)
                faces.append(resized_face.ravel())
                labels.append(user)

        faces = np.array(faces)
        knn = KNeighborsClassifier(n_neighbors=10)
        knn.fit(faces, labels)
        joblib.dump(knn, self.model_path)
        print('Model trained and saved as face_recognition_model.pkl')

    def add_user(self, new_user_name, new_user_id):
        user_img_folder = f'static/faces/{new_user_name}_{new_user_id}'

        if not os.path.isdir(user_img_folder):
            os.makedirs(user_img_folder)

        i, j = 0, 0
        cap = cv2.VideoCapture(0)

        while True:
            ret, frame = cap.read()

            if not ret:
                print("Failed to grab frame")
                break

            faces = self.extract_faces(frame)
            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 20), 2)
                cv2.putText(frame, f'Images Captured: {i}/{self.no_of_images}', (30, 30),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 20), 2, cv2.LINE_AA)
                if i < self.no_of_images:
                    if j % 5 == 0:
                        name = f'{new_user_name}_{i}.jpg'
                        cv2.imwrite(f'{user_img_folder}/{name}', frame[y:y+h, x:x+w])
                        i += 1
                j += 1

            cv2.imshow('Adding new User', frame)

            key = cv2.waitKey(1)
            if key == 27:  # ESC key pressed
                break
            elif i >= self.no_of_images:
                break

        cap.release()
        cv2.destroyAllWindows()

        print('Training Model')
        self.train_model()

    def start_recognition(self):
        if not os.path.isfile(self.model_path):
            print('There is no trained model in the static folder. Please add a new face to continue.')
            return

        cap = cv2.VideoCapture(0)
        img_background = np.zeros((500, 800, 3), dtype=np.uint8)
        img_background[:] = (0, 0, 0)  
        recognized_ids = set()

        while True:
            ret, frame = cap.read()
            if not ret:
                print("Failed to grab frame")
                break

            faces = self.extract_faces(frame)
            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x+w, y+h), (86, 32, 251), 1)
                face = cv2.resize(frame[y:y+h, x:x+w], self.face_size)

                identified_person = self.identify_face(face.reshape(1, -1))

                if identified_person.size > 0:
                    identified_person_str = str(identified_person[0])
                    name = identified_person_str.split('_')[0]
                    student_id = identified_person_str.split('_')[1]

                if student_id in recognized_ids:
                    print(f"Attendance for ID: {student_id} has already been marked.")
                else:
                    print(f"Marking attendance for ID: {student_id}")
                    if self.db_manager.add_attendance(f'{name}_{student_id}'):
                        recognized_ids.add(student_id)
                        print(f"Attendance successfully marked for: {name} (ID: {student_id})")
                    else:
                        print(f"Failed to mark attendance for ID: {student_id}")


                    cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 1)
                    cv2.putText(frame, f'{name}', (x, y-15), cv2.FONT_HERSHEY_COMPLEX, 0.7, (255, 255, 255), 1)

            resized_frame = cv2.resize(frame, (640, 350))
            img_background[125:125 + 350, 55:55 + 640] = resized_frame
            cv2.imshow('Attendance', img_background)

            key = cv2.waitKey(1)
            if key == 27:  # ESC key pressed
                break

        cap.release()
        cv2.destroyAllWindows()


db_manager = DatabaseManager()
face_recognition_system = FaceRecognitionSystem(db_manager)

@app.route('/')
def frontend():
    names, roll_no, _time, size = db_manager.extract_attendance()
    total_registered = len(os.listdir('static/faces'))
    return render_template('frontend.html', Name=names, Roll=roll_no, Time=_time)

@app.route('/start', methods=['GET'])
def start():
    face_recognition_system.start_recognition()
    return frontend()

@app.route('/add', methods=['GET', 'POST'])
def add():
    if request.method == 'POST':
        newusername = request.form['newusername']
        newuserid = request.form['newuserid']
        face_recognition_system.add_user(newusername, newuserid)
        return frontend()
    return render_template('frontend.html')

@app.route('/list', methods=['GET'])
def list_users():
    userlist, names, roll_no, size = db_manager.get_all_users()
    return render_template('list.html', user_list=userlist, Name=names, Roll_no=roll_no, size=size)


if __name__ == "__main__":
    app.run(debug=True)

