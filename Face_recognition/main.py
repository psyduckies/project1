import cv2
import face_recognition
from database import DatabaseManager
from facialrecognition import recognize_faces

def main():
    db_path = '/home/jhalak/jhalak/project1/Face_recognition/attendance.db'
    excel_path = '/home/jhalak/jhalak/project1/Face_recognition/attendance.xlsx'
    db_manager = DatabaseManager(db_path)

    video_capture = cv2.VideoCapture(0)

    # Ensure you have the correct image paths and file extensions
    image_of_student1 = face_recognition.load_image_file("/home/jhalak/jhalak/project1/Face_recognition/Attendance_Images/img_test.py.jpg")
    image_of_student2 = face_recognition.load_image_file("/home/jhalak/jhalak/project1/Face_recognition/Attendance_Images/img2.jpg")

    encoding_of_student1 = face_recognition.face_encodings(image_of_student1)[0]
    encoding_of_student2 = face_recognition.face_encodings(image_of_student2)[0]

    known_face_encodings = [encoding_of_student1, encoding_of_student2]
    known_face_names = ["Student Name 1", "Student Name 2"]

    while True:
        if not recognize_faces(video_capture, known_face_encodings, known_face_names, db_manager):
            break

    video_capture.release()
    cv2.destroyAllWindows()

    db_manager.export_to_excel(excel_path)
    db_manager.close_connection()

if __name__ == '__main__':
    main()

""" import cv2
from database import DatabaseManager
from facialrecognition import recognize_faces

def main():
    db_path = '/home/jhalak/jhalak/project1/Face_recognition/attendance.db'
    excel_path = '/home/jhalak/jhalak/project1/Face_recognition/attendance.xlsx'
    db_manager = DatabaseManager(db_path)

    video_capture = cv2.VideoCapture(0)

    known_face_encodings = []  # Load your known face encodings
    known_face_names = []      # Load the corresponding names
    image_of_student1 = face_recognition.load_image_file("/home/jhalak/jhalak/project1/Face_recognition/Attendance_Images/img_test.py.jpg")
    image_of_student2 = face_recognition.load_image_file("/home/jhalak/jhalak/project1/Face_recognition/Attendance_Images/img2.jpg")

    encoding_of_student1 = face_recognition.face_encodings(image_of_student1)[0]
    encoding_of_student2 = face_recognition.face_encodings(image_of_student2)[0]

    known_face_encodings.append(encoding_of_student1)
    known_face_names.append("Student Name 1")

    known_face_encodings.append(encoding_of_student2)
    known_face_names.append("Student Name 2")

    while True:
        if not recognize_faces(video_capture, known_face_encodings, known_face_names, db_manager):
            break

    video_capture.release()
    cv2.destroyAllWindows()

    
    db_manager.export_to_excel(excel_path)
    db_manager.close_connection()

if __name__ == '__main__':
    main()
    """
