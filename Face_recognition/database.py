import sqlite3

class DatabaseManager:
    def __init__(self, db_path):
        self.db_path = db_path
        self.connection = sqlite3.connect(db_path)
        self.cursor = self.connection.cursor()
        self.create_tables()  # Ensure tables are created on initialization

    def create_tables(self):
        
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS students (
                student_id INTEGER PRIMARY KEY,
                name TEXT NOT NULL UNIQUE
            )
        ''')

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS attendance (
                attendance_id INTEGER PRIMARY KEY,
                student_id INTEGER NOT NULL,
                time_in TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                time_out TIMESTAMP,
                attendance_status TEXT NOT NULL,
                FOREIGN KEY (student_id) REFERENCES students (student_id)
            ) 
        ''')
        self.connection.commit()

    def mark_attendance(self, student_id, attendance_status):
        try:
            if attendance_status == 'in':
                self.cursor.execute('''
                    INSERT INTO attendance (student_id, attendance_status)
                    VALUES (?, ?)
                ''', (student_id, attendance_status))
            elif attendance_status == 'out':
                self.cursor.execute('''
                    UPDATE attendance
                    SET time_out = CURRENT_TIMESTAMP
                    WHERE student_id = ? AND time_out IS NULL
                ''', (student_id,))
            else:
                raise ValueError("Invalid attendance status. Must be 'in' or 'out'.")
            
            self.connection.commit()
            return True
        except sqlite3.IntegrityError:
            print(f"Error: Student with ID {student_id} not found.")
            return False
        except ValueError as ve:
            print(ve)
            return False

    def get_student_id_by_name(self, name):
        self.cursor.execute('''
            SELECT student_id FROM students WHERE name = ?
        ''', (name,))
        result = self.cursor.fetchone()
        if result:
            return result[0]
        else:
            return None

    def close_connection(self):
        self.connection.close()

    def export_to_excel(self, excel_path):
        import pandas as pd
        query = '''
            SELECT s.name, a.time_in, a.time_out, a.attendance_status
            FROM attendance a
            INNER JOIN students s ON a.student_id = s.student_id
        '''
        df = pd.read_sql_query(query, self.connection)
        df.to_excel(excel_path, index=False)
