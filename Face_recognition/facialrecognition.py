import face_recognition
import cv2
from database import DatabaseManager

def recognize_faces(video_capture, known_face_encodings, known_face_names, db_manager):
    ret, frame = video_capture.read()
    rgb_frame = frame[:, :, ::-1]  # Convert BGR to RGB

    face_locations = face_recognition.face_locations(rgb_frame)
    face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)

    for face_encoding in face_encodings:
        matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
        name = "Unknown"

        if True in matches:
            first_match_index = matches.index(True)
            name = known_face_names[first_match_index]

            # Mark attendance with "time in"
            recognize_and_mark_attendance(db_manager, name, 'in')

    cv2.imshow('Video', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        return False  
    return True  

def recognize_and_mark_attendance(db_manager, student_name, status):
    student_id = db_manager.get_student_id_by_name(student_name)
    if student_id is not None:
        db_manager.mark_attendance(student_id, status)
    else:
        print(f"Student {student_name} not found in database.")

