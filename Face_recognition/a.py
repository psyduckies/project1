import cv2

cap = cv2.VideoCapture(1)  # Try different index if 0 doesn't work

if not cap.isOpened():
    print("Error: Camera not accessible.")
else:
    print("Camera is working.")
    cap.release()

