# Project1
# AttendEase

AttendEase is a face recognition-based attendance marking system designed to streamline the process of recording student attendance in educational institutions. Leveraging advanced facial recognition technology, AttendEase offers a convenient and efficient way to track attendance without the need for manual input or traditional methods like roll calls.

## Features

- **Face Recognition**: Utilizes facial recognition algorithms to identify and mark students present based on their enrolled profiles.

- **Real-time Updates**: Attendance records are updated in real-time, providing instant visibility to administrators and instructors.

- **Accuracy**: Ensures accurate attendance records while maintaining data .

- **User-friendly Interface**: Simple and intuitive interface for both administrators and users, making it easy to operate and manage.

- **Excel Export**: Attendance records can be exported to an Excel sheet, including student names, entry times, and exit times.

## How It Works

AttendEase works by capturing live images of students as they enter the designated attendance area. These images are then compared against the database of enrolled student profiles using facial recognition algorithms. Upon successful recognition, the system marks the student as present for the session.

## Installation and Setup

To set up AttendEase, follow these steps:

1. **Clone the Repository**: Clone the AttendEase repository from https://gitlab.com/psyduckies/project1.git.

2. **Install Dependencies**: Install the necessary Python packages. These include Flask, OpenCV, Pandas, Joblib, and NumPy.

3. **Configure Database**: Set up and configure the database where student profiles and attendance records will be stored.

4. **Run the Application**: Start the AttendEase application and configure it according to your institution's requirements.

## Usage
- **Marking Attendance**: Students press the "Mark Attendance" button on the screen, and the system automatically handles the recognition and recording process.
- **Adding New Users**: If a student is not recognized, the "Add New User" option allows for entering the student's details, including their facial profile, ensuring they are enrolled for future sessions.

